<?php
declare(strict_types = 1);
/**
 * This file is part of quadrixo/web-application library
 *
 * PHP version 7.3
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @license https://cecill.info/licences/Licence_CeCILL_V2.1-en.txt CeCILL-2.1
 * @author Luc Chante <luc.chante@gmail.com>
 * @copyright 2020 Luc Chante - All rights reserved
 */
namespace Quadrixo\WebApp\Internal;

use Closure;
use Fig\Http\Message\StatusCodeInterface as StatusCode;
use Generator;
use InvalidArgumentException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Quadrixo\ApplicationBuilder;

trait PipelineBuilderTrait
{
    private $middlewares = [];

    public abstract function getContainer(): ContainerInterface;

    /**
     * Append the given middleware into the pipeline.
     *
     * If `$config` is specified, it is called before middleware resolution.
     *
     * @param string|MiddlewareInterface|callable $middleware
     * @param Closure $configure
     * @return ApplicationBuilder
     */
    public function use($middleware): ApplicationBuilder
    {
        if ($middleware instanceof MiddlewareInterface
            || $middleware instanceof Closure
            || (is_string($middleware) && $this->getContainer()->has($middleware) && is_subclass_of($middleware, MiddlewareInterface::class))
            || is_callable($middleware)
        )
        {
            $this->middlewares[] = $middleware;
        }
        else
        {
            $type = is_string($middleware) ? $middleware : get_debug_type($middleware, true);
            throw new InvalidArgumentException("'$type' is not a valid type for a middleware");
        }

        return $this;
    }

    /**
     * Returns an handler for HTTP requests using middelwares
     *
     * @return RequestHandlerInterface
     */
    public function build(): RequestHandlerInterface
    {
        $pipelineGenerator = $this->generate();
        return (new class($pipelineGenerator) extends PipelineExecutor { })
            ->setContainer($this->getContainer());
    }

    private function generate(): Generator
    {
        yield from $this->middlewares;

        $honeyPot = function(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
        {
            /** @var ResponseFactoryInterface */
            $factory = $this->get(ResponseFactoryInterface::class);
            return $factory->createResponse(StatusCode::STATUS_NOT_FOUND);
        };
        return $honeyPot;
    }
}
